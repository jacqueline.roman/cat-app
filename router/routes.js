import {createRouter, createWebHashHistory} from "vue-router";
import CatFacts from '../components/CatFacts';
import UserForm from '../components/UserForm';

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {path: '/',component: UserForm},
        {path:'/cats', component: CatFacts},
        {path: '/user-form',component: UserForm},
    ]
});

export default router;

