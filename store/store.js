import { createStore } from 'vuex'

export default createStore({
  state: {
    userName: {
      firstName: '',
      lastName: ''
    }
  },
  getters: {
  },
  mutations: {
    setFirstName: (state, payload) => {
      state.userName.firstName = payload;
    },
    setLastName: (state, payload) =>{
      state.userName.lastName = payload;
    }
  },
  actions: {
  },
  modules: {
  }
})
